import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { HttpClient } from '@angular/common/http';
import Student from '../entity/student';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentRestImplService extends StudentService{

  saveStudent(student: Student): Observable<Student> {
    return this.http.post<Student>(environment.studentApi, student);
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(environment.studentApi);
  }

  getStudent(id: number): Observable<Student> {
    return this.http.get<Student>(environment.studentApi + '/' + id);
  }

  constructor(private http: HttpClient) {
    super();
   }

}
